des.blog = {
	init: function(context) {
	},

	init_hover_articles: function(context) {
		var articles = $('#blog article', context);
		articles.each(function(i,v) {
			$(v).hover(function() {
				$(v).addClass('hover');
			}, function() {
				$(v).removeClass('hover');				
			});
		});	
	}
}
