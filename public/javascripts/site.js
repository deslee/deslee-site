UTIL = {
	fire: function(func, funcname, args) {
		var namespace = des;

		if (funcname === undefined)
			funcname = 'init';

		if (namespace && namespace[func] && namespace[func][funcname]
					&& typeof namespace[func][funcname] == 'function') {
		console.log('firing des' +  '.' + func + '.' + funcname);
		namespace[func][funcname](args);
		}
	},

	loadEvents: function() {
		var bodyId = document.body.id;

		UTIL.fire('common', 'init', document);

		var actions = $('#content[data-actions]').data('actions');
		for(var i in actions) {
			var action = actions[i];
			UTIL.fire(action);
		}

		UTIL.fire('common', 'finalize', document);
	}
}

des = {

};

des.common = {
	init: function(context) {
		if (context == document) {
			des.common.fadedelay();
			window.setTimeout(function() {
				des.common.affix_bottom_content();
			}, 1);
			$(window).resize(des.common.affix_bottom_content);
		}
		//des.common.init_popovers(context);
	},

	affix_bottom_content: function() {
		var bottom = $('#bottom').removeClass('stick');
		if (bottom.offset().top+30 < $(window).height()) {
			bottom.addClass('stick');
		}
	},

	fadedelay: function(context) {
		$('body').addClass('invisible').each(function(i,e){
			$(this).delay(25*i).removeClass('invisible').hide().fadeIn('slow');
		});
	},

	init_popovers: function(context) {
		$('[data-popover-action]', context).each(function(index, element) {
			var ele = $(element),
			action = ele.data('popover-action'),
			popover = $('[data-popover="' + action + '"]');

			if (context == document) {
				ele.popover({
					html: true,
					content: popover.html(),				
					placement: 'top',
					trigger: 'click',
					container: 'html',
				});
			}
		});
	},
};

jQuery(document).ready(function() {
	UTIL.loadEvents();
});
