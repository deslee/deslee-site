/**
var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server;

BlogService = function() {
	var self = this;
	MongoClient.connect("mongodb://localhost:27017/blog", function(err, db) {
		console.log('connected');
		self.db = db;
	});
};

BlogService.prototype.findAll = function(callback) {
	var self = this;
	self.db.collection('blog').find().toArray(
		function(err,res) {
			callback(res);
		}
	);
}
**/

BlogService = function() {
	this.fs = require('fs');
	this.path = './blog/';
	this.jade = require('jade');
};

BlogService.prototype.findAll = function(callback) {
	var self = this;
	self.fs.readdir(self.path, function(err, fileNames) {
		if(err) {
			console.log(err);
		}
		var files = [];
		fileNames.forEach(function(fileName) {
			try {
			var fileContent = self.fs.readFileSync(self.path + fileName, 'utf-8');
			var index = fileContent.indexOf('}');
			
			object = JSON.parse(fileContent.substr(0, index+1));
			object.date = new Date(object.date);
			object.content = self.jade.render(fileContent.substr(index+1), {filename: "./views/main.jade"});
			object.slug = fileName;

			if (object.publish) {
				files.push(object);
			}
			}
			catch(err)
			{
				console.log(err);
			}
		});
		files.sort(function(a, b) {
			return b.date.getTime() - a.date.getTime();				
		})
		callback(files);
	});
};

BlogService.prototype.getEntryBySlug = function(slug, callback) {
	var self = this;
	var func = function(entries) {
		for(var i in entries) {
			var element = entries[i];
			if (element.slug === slug) {
				callback(element);
				return;
			}
		}
	}
	self.findAll(func);
}



exports.BlogService = BlogService;
