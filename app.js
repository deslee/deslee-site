
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var BlogService = require('./blog').BlogService;

var app = express();
var blogService = new BlogService();

//var render = function(res, name, obj, additionalActions, path) {
var render = function(res, options, obj) {
	obj = obj ? obj : [];
	obj.action = options.name;
	obj.actions = [options.name];

	var additionalActions =	options.additionalActions;

	if (additionalActions &&
			additionalActions.length &&
			typeof additionalActions === 'object') {
		obj.action = obj.actions.concat(additionalActions);
	} else if (typeof additionalActions === 'string') {
		obj.actions.push(additionalActions);
	}

	var path = options.path != undefined ? path + '/' : '';

	res.render(path + options.name, obj);
}

var simpleView = function(names) {
	for (var i in names) {
		var data = names[i],
		route = '/' + data.name;

		var register = function(route, name, path) {
			console.log('registering ' + route + ' with ' + name);
			app.get(route, function(req, res) {
				render(res, 
				{
					name: name,
					path: path,
				}, {
					title: name.charAt(0).toUpperCase() + name.substr(1)
				});
			});
		};

		register(route, data.name, data.path);
	}
}


// all environments
app.set('port', process.env.PORT || 80);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger(':remote-addr - :method :url :status - :response-time ms'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/users', user.list);

app.get('/', function(req, res) {
	blogService.findAll(function(array){
		render(res, 
		{
			name: 'index',
			additionalActions: 'blog',
		},
		{
			title: 'Desmond Lee',
			blog: array	
		})
	});
});

app.get('/', routes.index);

app.get('/archive', function(req, res) {
	blogService.findAll(function(all) {
		render(res, 
		{
			name: 'archive',
		},
		{
			title: "Archive",
			blog: all,
		});
	});
});

app.get('/blog/*', function(req, res) {
	var slug = req.params[0];
	if (slug !== undefined) {
		blogService.getEntryBySlug(slug, function(entry) {
			render(res,
			{
				name: 'entry',
			}, 
			{
				title: entry.title,
				entry: entry
			})
		});
	}
});

simpleView([
{name: 'resume'},
{name: 'contact'},
{name: 'catpage', path: 'projects'},
]);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
